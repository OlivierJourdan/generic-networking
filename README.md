# Generic Networking Singleton Module

Ever wanted to use the API of a DQMH module in an application when the module is actually running in another application on another device, connected via network? Now you can. 

The motivation behind this implementation is to use the very same API of a module on different, distributed systems. Both the caller and the callee are absolutely oblivious of whether they are running on the same or different systems.

## Example Code

The `\Generic Networking Example` directory contains an example project with two modules:

### GenNet-Proxy 

The `GenNet-Proxy` module is a DQMH Singleton module altered to allow for (near) zero-coupled networking functionality.

- Call the *Enable Network Forwarding* request to make this module a proxy. By loading a GenNet-Client clone module and redirecting communication, all requests are sent via TCP to another `GenNet-Proxy.lvlib` running in another application and/or another PC accessible via TCP/IP.

- Call the *Enable Network Listening* request to make this module accept messages via network. By loading a GenNet-Server clone module, let this module accept messages from `GenNet-Proxy.lvlib` running in another application and/or on another PC via TCP/IP communication.

### GenNet-RoundTrip
The `GenNet-RoundTrip` module is like the `GenNet-Proxy` module, but with a new feature for sending DQMH `Round Trip` events over the network. The normal `Round Trip` event also includes a DQMH broadcast, but this broadcast will only be fired locally. With the new feature, the normal broadcast will be encapsulated in a generic broadcast and the remote side can identify it. The `GenNet-RoundTrip` module showcases the concrete implementation.

### RemoteControl

The `RemoteControl` module showcases the manual use of the GenNet-Client and GenNet-Server modules, and how any generic DQMH module can make use of the generic networking functions, allbeit not transparently to the user. The message that's sent via the network needs to be constructed manually, and in a way that the opposite site can decode it. 

## Test

1. Open the `\Generic Networking Example\Generic Networking Example.lvproj` file 

2. Start the `Test GenNet-Proxy API.vi` Tester, start the module and *Enable Network Listening*

3. Start the `Test RemoteControl API.vi` Tester, start the module and *Open GenNet Connection* 

Now, you have a network connection between the two modules.

4. If you *Send Message via GenNet* in the `Test RemoteControl API.vi` Tester, it will actually call the *Do something with answer* request of the `GenNet-Proxy` module. 

5. Now, *Update Factor* in the `Test GenNet-Proxy API.vi` Tester and see how the result in the `Test RemoteControl API.vi` Tester changes when you call *Send Message via GenNet* again.


## Documentation

For detailed documentation on how the actual implementation works, see the `\Generic Networking Example\_doc` directory.

You can also read the blog post we published on the Delacor Blog at https://delacor.com/dqmh-generic-networking-module/

## Credits

These people contributed - buy them beer if you meet them!

* Florian Batz, innofas GmbH
* Hampel Software Engineering

## License

See the LICENSE file in the root directory of the repo.

## Contact

The Modules and Module Templates in this repository are created and provided by HAMPEL SOFTWARE ENGINEERING and are built for and based on the Delacor Queued Message Handler (DQMH, www.delacor.com).

Please get in touch with us at (office@hampel-soft.com) or visit our website (www.hampel-soft.com)
